# ----------------------------------------------------------------------------#
#              OPTAIN SCENARIOS FIELD SCALE                                   #
#
#                     https://www.optain.eu/
#
#  Date: 13/09/2023
#
# Authors: Maria Eliza Turek
# <mariaeliza.turek@agroscope.admin.ch>
#
# ---------------------------------------------------------------------------#

#  ------------------------------------------------------------------------ #
#
#        Part III - comparing soil water balance among all scenarios
#
#  ------------------------------------------------------------------------ #

wdynamics = data.frame()
for (i in 1:length(swap.case)) {
#for (i in 1:4) {
  vap = read.csv(file = paste0("../swap_files/rcp",rcp[r],'/',model.chain[m],'_',swap.case[i],"/result.inc"),
                 header = TRUE, as.is = TRUE, skip = 6)
  vap$Date = as.Date(vap$Date) # ,format="%d-%b-%Y")
  vap$Year = format(vap$Date, "%Y")
  vap$month = format(vap$Date, "%m")
  vap$doy = format(vap$Date, "%j")
  
  vap.year = vap  %>%
    dplyr::group_by(Year)%>%
    dplyr::summarise(Rain_mean     = sum(Rain), 
                     Interc_mean   = sum(Interc),
                     Runoff_mean   = sum(Runoff),
                     Tpot_mean     = sum(Tpot),
                     Tact_mean     = sum(Tact),
                     Epot_mean     = sum(Epot),
                     Eact_mean     = sum(Eact),
                     Drainage_mean = sum(Drainage),
                     QBottom_mean  = sum(QBottom),
                     dstorage_mean = sum(dstorage))
  vap.year$case = factor(swap.case[i])
  
  vap.year = vap.year   %>%
  dplyr::mutate(period =
                  dplyr::case_when(Year <= 2022 ~  "A",
                                  (Year >= 2040 & Year<2060) ~ "B",
                                   Year >= 2080 ~  "C"))
  
  wdynamics = rbind(wdynamics, vap.year)
 
    
}

levels(wdynamics$case) = measures
wdynamics$period <- as.factor(wdynamics$period)
levels(wdynamics$period) = c("Current conditions","Mid-century","End of Century")

#  ------------------------------------------------------------------------ #
#
#                   Plot the results 0 - 25 cm soil depth
#
#  ------------------------------------------------------------------------ #

p_theme =  theme_bw(base_size = 14) +
  theme(axis.title.x = element_blank(),
        axis.text.x  = element_text(),
        #legend.position = 'bottom'
  )

var = c("Rain","Interc","Runoff","Tpot","Tact","Epot","Eact","Drainage","QBottom",
        "dstorage")
i=1
for (i in 1:length(var)) {
  
  df = wdynamics %>%
    dplyr::select(Year,case,period, starts_with(var[i]))
  
  colnames(df)[4] = "vari"
  
  
  p1 = df %>%
    ggplot( aes(x=period, y=vari)) + 
    geom_boxplot(aes(fill=as.factor(case)),width = 0.5, lwd=0.1,outlier.shape = 21, outlier.stroke = 0.25)+
    scale_y_continuous(name = expression(paste(" (", cm,~y^{-1},")")))+
    # facet_wrap(~local)+
    labs(title = paste0(var[i]))+
    # #scale_fill_brewer(palette="Dark2")+
    scale_fill_manual("Measure",values = c("coral4","darkgoldenrod4","darkorchid4","chartreuse4",
                                           "coral3","darkgoldenrod3","darkorchid3","chartreuse3",
                                           "coral2","darkgoldenrod2","orchid4","chartreuse2",
                                           "sienna1","yellow3","orchid","greenyellow")
                      # labels =
                      #   c( "actual_conv", "actual_drcrop","actual_grass",
                      #      "actual_ntill","ww_conv","ww_ntill",
                      #      "wd_conv","wd_drcrop","ip_grass", "ip_conv"  )
    )+
    p_theme
  p1
  plotfile = paste0("../swap_files/plots/rcp",rcp[r],'/',model.chain[m],'/','water_balance_',var[i],model.chain[m],".png")
  png(plotfile, width = 1100, height = 700, res = 150)
  print(p1)
  dev.off()
  
  
}



#  ------------------------------------------------------------------------ #
#
#                            the end 
#
#  ------------------------------------------------------------------------ #
